---
title: Upkeep
---

An important aspect of open hardware is that you have the information you need so you can fix it - if you have the skills to do so.
In this section hopefully we can collectively gather information and tutorials so all of us can enjoy our card10s for as long as possible!

Generate a card10 case design for 3D printing to protect your card10: https://card10-cover.0x1b.de/index.php
