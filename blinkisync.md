### blinkisync

From firmware version 1.8 you will be able to send and receive text via light with your card10.

You can exchange contacts, exchange secret notes, embed messages into your LED spotlights, ...

#### usage of the app
Choose blinkisync-receive for receiving transmissions and blinkisync-transmit for transmitting text. 

Start interaction by holding the right upper button. Then hold the flashing LED of the sending Card10 to the ambient light sensor of the receiving card10.
the ambient light sensor is located above the "d" of the card10 logo.

The receiving card10 will calibrate now, which means that it measures maximum and minimum of received light in the last second.

When the shown values are stable, the button of the receiving card10 can be let go and after that, the button of the sending card10 can be let go.

Transmission will start now. After transmission the received text can be saved to `blinki-received.txt`

The string to be transmitted will be pulled from `blinki-transmission.txt`

#### Protocol:

Text gets decoded to zero padded utf-8 bytes and bits translated to LED on or off with a timing of 150ms.
In front of the data bytes is a special start byte and there is a timing signal after each byte. The end signal is 8 bits of 0.

Start byte:
`00000101`

Timing byte:
`1011`

End byte:
`00000000`

Example:
hello world:
`00000101 1011 01101000 1011 01100101 1011 01101100 1011 01101100 1011 01101111 1011 00100000 1011 01110111 1011 01101111 1011 01110010 1011 01101100 1011 01100100 1011 00000000`
